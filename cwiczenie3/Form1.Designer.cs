﻿namespace cwiczenie3
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtPierwsza = new System.Windows.Forms.TextBox();
            this.txtDruga = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnPolicz = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtPierwsza
            // 
            this.txtPierwsza.Location = new System.Drawing.Point(191, 24);
            this.txtPierwsza.Name = "txtPierwsza";
            this.txtPierwsza.Size = new System.Drawing.Size(100, 20);
            this.txtPierwsza.TabIndex = 0;
            // 
            // txtDruga
            // 
            this.txtDruga.Location = new System.Drawing.Point(191, 60);
            this.txtDruga.Name = "txtDruga";
            this.txtDruga.Size = new System.Drawing.Size(100, 20);
            this.txtDruga.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(106, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Pierwsza liczba";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(119, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Druga liczba";
            // 
            // btnPolicz
            // 
            this.btnPolicz.Location = new System.Drawing.Point(151, 97);
            this.btnPolicz.Name = "btnPolicz";
            this.btnPolicz.Size = new System.Drawing.Size(75, 23);
            this.btnPolicz.TabIndex = 4;
            this.btnPolicz.Text = "Policz NWD";
            this.btnPolicz.UseVisualStyleBackColor = true;
            this.btnPolicz.Click += new System.EventHandler(this.btnPolicz_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(408, 141);
            this.Controls.Add(this.btnPolicz);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtDruga);
            this.Controls.Add(this.txtPierwsza);
            this.Name = "Form1";
            this.Text = "Największy wspólny dzielnik - algorytm Euklidesa";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtPierwsza;
        private System.Windows.Forms.TextBox txtDruga;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnPolicz;
    }
}

