﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cwiczenie3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        // Napisz program w WinForms, który naiwnie będzie sprawdzać największy wspólny dzielnik
        // dwóch podanych  liczb algorytmem Euklidesa.
        // https://pl.wikipedia.org/wiki/Algorytm_Euklidesa
        // Wyświetlić wynik NWD w MessageBox (najlepiej z ikonką info)

        private void btnPolicz_Click(object sender, EventArgs e)
        {
            int pierwsza = int.Parse(txtPierwsza.Text);
            int druga = int.Parse(txtDruga.Text);
            while (pierwsza != druga)
            {
                if (pierwsza > druga)
                    pierwsza -= druga;
                else
                    druga -= pierwsza;
            }
            string trescMessageBoxa = String.Format("Największy wspólny dzielnik liczb {0} oraz {1} to: {2}", txtPierwsza.Text, txtDruga.Text, pierwsza);
            MessageBox.Show(trescMessageBoxa, "Wynik algorytmu", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
